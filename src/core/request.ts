import { Event, SentryRequest } from '@sentry/types';

import { CustomizedAPI } from './api';

/** Extract sdk info from from the API metadata */
/**
 * Apply SdkInfo (name, version, packages, integrations) to the corresponding event key.
 * Merge with existing data if any.
 **/

/** Creates a SentryRequest from an event. */
export function eventToSentryRequest(event: Event, api: CustomizedAPI): SentryRequest {
  const eventType = event.type || 'event';
  const useEnvelope = eventType === 'transaction';

  const { transactionSampling, ...metadata } = event.debug_meta || {};
  const { method: samplingMethod, rate: sampleRate } = transactionSampling || {};
  if (Object.keys(metadata).length === 0) {
    delete event.debug_meta;
  } else {
    event.debug_meta = metadata;
  }

  const req: SentryRequest = {
    body: JSON.stringify(event),
    type: eventType,
    url: api.getEndpointWithUrlEncodedAuth(),
  };

  // https://develop.sentry.dev/sdk/envelopes/

  // Since we don't need to manipulate envelopes nor store them, there is no
  // exported concept of an Envelope with operations including serialization and
  // deserialization. Instead, we only implement a minimal subset of the spec to
  // serialize events inline here.
  if (useEnvelope) {
    const envelopeHeaders = JSON.stringify({
      event_id: event.event_id,
      sent_at: new Date().toISOString(),
    });
    const itemHeaders = JSON.stringify({
      type: event.type,

      // TODO: Right now, sampleRate may or may not be defined (it won't be in the cases of inheritance and
      // explicitly-set sampling decisions). Are we good with that?
      sample_rates: [{ id: samplingMethod, rate: sampleRate }],

      // The content-type is assumed to be 'application/json' and not part of
      // the current spec for transaction items, so we don't bloat the request
      // body with it.
      //
      // content_type: 'application/json',
      //
      // The length is optional. It must be the number of bytes in req.Body
      // encoded as UTF-8. Since the server can figure this out and would
      // otherwise refuse events that report the length incorrectly, we decided
      // not to send the length to avoid problems related to reporting the wrong
      // size and to reduce request body size.
      //
      // length: new TextEncoder().encode(req.body).length,
    });
    const envelope = `${envelopeHeaders}\n${itemHeaders}\n${req.body}`;
    req.body = envelope;
  }

  return req;
}

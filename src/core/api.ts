import { DsnLike } from '@sentry/types';
import { urlEncode } from '@sentry/utils';
import { Dsn } from '../utils/dsn'

/**
 * Helper class to provide urls, headers and metadata that can be used to form
 * different types of requests to Sentry endpoints.
 * Supports both envelopes and regular event requests.
 **/
export class CustomizedAPI {

  dsnObject: Dsn
  public constructor(dsn: DsnLike) {
    this.dsnObject = new Dsn(dsn)
  }


  /** Returns the prefix to construct Sentry ingestion API endpoints. */
  public getBaseApiEndpoint(): string {
    const dsn = this.dsnObject;
    const protocol = dsn.protocol ? `${dsn.protocol}:` : '';
    const port = dsn.port ? `:${dsn.port}` : '';
    return `${protocol}//${dsn.host}${port}${dsn.path ? `/${dsn.path}` : ''}/`;
  }

  public getEndpointWithUrlEncodedAuth(): string {
    return `${this.getEndpoint()}?${this.encodedAuth()}`;
  }

  private getIngestEndpoint() {
    const base = this.getBaseApiEndpoint();
    const dsn = this.dsnObject;
    return `${base}${dsn.projectId}/eventLogs/`;
  }

  public getEndpoint(): string {
    return this.getIngestEndpoint();
  }

  private encodedAuth(): string {
    const dsn = this.dsnObject;
    const auth = {
      [dsn.publicKey]: dsn.pass,
    };
    return urlEncode(auth);
  }

  /**
   * Returns the envelope endpoint URL with auth in the query string.
   *
   * Sending auth as part of the query string and not as custom HTTP headers avoids CORS preflight requests.
   */

  /** Returns only the path component for the store endpoint. */
  public getStoreEndpointPath(): string {
    const dsn = this.dsnObject;
    return `${dsn.path ? `/${dsn.path}` : ''}/${dsn.projectId}/eventLogs`;
  }

  /**
   * Returns an object that can be used in request headers.
   * This is needed for node and the old /store endpoint in sentry
   */
  public getRequestHeaders(): { [key: string]: string } {
    // CHANGE THIS to use metadata but keep clientName and clientVersion compatible
    const dsn = this.dsnObject;

    if (!dsn.publicKey || !dsn.pass) {
      throw new Error('Authorization')
    }

    const headers = {
      'Content-Type': 'application/json',
      'sentry_apikey': `${dsn.publicKey}-${dsn.pass.toString()}`,
    }

    return  headers
  }

}
